<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name')->unique();
            $table->string('slug')->unique();
            $table->longText('content');
            $table->tinyInteger('status')->comment('1 For Published,0 For Pending')->default(1);
            $table->string('url')->comment('Complete URL of published post');
            $table->string('attachments');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('posts');
	}

}
