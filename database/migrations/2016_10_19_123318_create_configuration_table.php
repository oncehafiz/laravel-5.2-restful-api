<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigurationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('configuration', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('logo')->comment('Logo of the application');
            $table->string('copyright')->comment('Footer copyright message');
            $table->string('slogan')->comment('Slogan if any');
            $table->string('social_fb')->comment('Facebook profile link');
            $table->string('social_tw')->comment('Twitter profile link');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('configuration');
	}

}
