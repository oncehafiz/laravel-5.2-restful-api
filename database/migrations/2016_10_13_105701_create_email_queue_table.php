<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailQueueTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('email_queue', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('sender_name')->comment('Sender Name');
            $table->string('sender_email');
            $table->string('reciver_name')->comment('Reciver Name');
            $table->string('reciver_email');
            $table->string('subject')->comment('Subject of email');
            $table->longText('body')->comment('HTML Body of email');
            $table->tinyInteger('status')->comment('1 For Sent,0 For Pending')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('email_queue');
	}

}
