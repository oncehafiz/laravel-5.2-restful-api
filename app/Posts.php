<?php namespace Search;
use Swagger\Annotations as SWG;
use Illuminate\Database\Eloquent\Model;

/**
 * @SWG\Model(
 * 	id="Posts",
 * 	@SWG\Property(name="id", type="integer", required=true),
 * 	@SWG\Property(name="slug", type="string", required=true),
 * 	@SWG\Property(name="name", type="string", required=true),
 * 	@SWG\Property(name="content", type="string", required=true),
 *  @SWG\Property(name="attachments", type="string", required=false),
 *  @SWG\Property(name="status", type="integer", required=true),
 *  @SWG\Property(name="url", type="string", required=false),
 * )
 */

class Posts extends Model {

    protected $table = 'posts';
    protected $fillable = ['slug','name','content','attachments','url','status'];
    protected static $POST_PUBLISH = 1;

    /**
     * Get the book that owns the author.
     */
    public function comments()
    {
        return $this->hasMany('App\Comments');
    }

}
