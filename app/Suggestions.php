<?php namespace Search;
use Swagger\Annotations as SWG;

use Illuminate\Database\Eloquent\Model;

/**
 * @SWG\Model(
 * 	id="Suggestion",
 * 	@SWG\Property(name="id", type="integer", required=true),
 * 	@SWG\Property(name="slug", type="string", required=true),
 * 	@SWG\Property(name="name", type="string", required=true),
 * 	@SWG\Property(name="email", type="string", required=true),
 * 	@SWG\Property(name="content", type="string", required=true),
 *  @SWG\Property(name="ticket_id", type="string", required=true),
 *  @SWG\Property(name="attachments", type="string", required=false),
 * )
 */
class Suggestions extends Model {

    protected $table = 'suggestions';
    protected $fillable = ['name','email','content','attachments','slug','ticket_id'];

}
