<?php namespace Search;
use Swagger\Annotations as SWG;
use Illuminate\Database\Eloquent\Model;

/**
 * @SWG\Model(
 * 	id="Books",
 * 	@SWG\Property(name="id", type="integer", required=true),
 * 	@SWG\Property(name="slug", type="string", required=true),
 * 	@SWG\Property(name="name", type="string", required=true),
 * 	@SWG\Property(name="author_id", type="string", required=true),
 *  @SWG\Property(name="attachments", type="string", required=false),
 * )
 */
class Books extends Model {

    protected $table = 'books';
    protected $fillable = ['slug','name','author_id','attachments'];

    /**
     * Get the authors for the book.
     */
    public function authors()
    {
        return $this->hasMany('App\Authors');
    }
}
