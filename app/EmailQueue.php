<?php namespace Search;

use Illuminate\Database\Eloquent\Model;

class EmailQueue extends Model {

    protected $table    = 'email_queue';
    protected $fillable = ['sender_name','sender_email','reciver_name','reciver_email','subject','body','status'];
}
