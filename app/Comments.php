<?php namespace Search;
use Swagger\Annotations as SWG;
use Illuminate\Database\Eloquent\Model;

/**
 * @SWG\Model(
 * 	id="Comments",
 * 	@SWG\Property(name="id", type="integer", required=true),
 * 	@SWG\Property(name="slug", type="string", required=true),
 * 	@SWG\Property(name="comment", type="string", required=true),
 * 	@SWG\Property(name="post_id", type="integer", required=true),
 *  @SWG\Property(name="attachments", type="string", required=false),
 * )
 */


class Comments extends Model {

    protected $table = 'comments';
    protected $fillable = ['slug','comment','post_id','attachments'];

    /**
     * Get the post for the comment.
     */
    public function posts()
    {
        return $this->belongsTo('App\Posts');
    }
}
