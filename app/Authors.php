<?php namespace Search;
use Swagger\Annotations as SWG;
use Illuminate\Database\Eloquent\Model;

/**
 * @SWG\Model(
 * 	id="Authors",
 * 	@SWG\Property(name="id", type="integer", required=true),
 * 	@SWG\Property(name="slug", type="string", required=true),
 * 	@SWG\Property(name="name", type="string", required=true),
 * 	@SWG\Property(name="email", type="string", required=true),
 *  @SWG\Property(name="attachments", type="string", required=false),
 * )
 */
class Authors extends Model {

    protected $table = 'authors';
    protected $fillable = ['slug','name','email','attachments'];


    /**
     * Get the book that owns the author.
     */
    public function books()
    {
        return $this->belongsTo('App\Books');
    }
}
