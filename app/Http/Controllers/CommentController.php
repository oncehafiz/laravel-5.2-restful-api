<?php namespace Search\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
use Search\Http\Requests;
use Search\Comments;
use Illuminate\Http\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Swagger\Annotations as SWG;


/**
 * @SWG\Resource(
 *    apiVersion="1.0",
 *    basePath="http://search.local/api/v1",
 *    resourcePath="/comments",
 *    description="Comments operations",
 *    produces="['application/json']"
 * )
 */

class CommentController extends Controller {

    /**
     * @SWG\Api(
     *    path="/comments",
     *      @SWG\Operation(
     *        method="GET",
     *        summary="Display a listing of the resource.",
     *		@SWG\ResponseMessage(code=404, message="Invalid request"),
     *      @SWG\ResponseMessage(code=422, message="Basic validation failed"),
     *    )
     * )
     */
	public function index()
	{
        try {

            $response['status'] = 'success';
            $response['comments'] = [];

            $statusCode = 200;
            $comments = Comments::all();

            foreach ($comments as $comment) {
                $response['comments'][] = $comment;
            }

        } catch (\Exception $e) {
            $response['status'] = 'error';
            $response['comments'] = [];

            $statusCode = 404;
        } finally {
            return Response()->json($response, $statusCode);
        }
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

    /**
     * @SWG\Api(
     *    path="/comments/{id}",
     *      @SWG\Operation(
     *        method="GET",
     *        summary="Display the specified resource.",
     *		@SWG\Parameter(
     *            name="id",
     *            description="id of comment to fetch",
     *            paramType="path",
     *            required=true,
     *            allowMultiple=false,
     *            type="integer"
     *        ),
     *		@SWG\ResponseMessage(code=400, message="Comment not found"),
     *      @SWG\ResponseMessage(code=422, message="Comment not found"),
     *      @SWG\ResponseMessage(code=200, message="Comment Found"),
     *    )
     * )
     */
	public function show($id)
	{
        try {
            $posts = Comments::findOrFail($id);
            $response['comments'] = $posts;
            $response['status'] = 'success';
            $response['message'] = '';
            $statusCode = 200;
        } catch (ModelNotFoundException $e) {
            $response['comments'] = [];
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
            $response['file'] = $e->getFile();
            $response['line'] = $e->getLine();
            $statusCode = 400;
        }
        return Response()->json($response, $statusCode);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

    /**
     * @SWG\Api(
     *    path="/comments/{id}",
     *      @SWG\Operation(
     *        method="DELETE",
     *        summary=" Remove the specified resource from storage.",
     *		@SWG\Parameter(
     *            name="id",
     *            description="id of comment to fetch and update",
     *            paramType="path",
     *            required=true,
     *            allowMultiple=false,
     *            type="integer"
     *        ),
     *		@SWG\ResponseMessage(code=400, message="Comment not found"),
     *      @SWG\ResponseMessage(code=422, message="Comment not found"),
     *     @SWG\ResponseMessage(code=200, message="Comment Deleted"),
     *    )
     * )
     */
    public function destroy($id)
    {
        try {
            $comment = Comments::findOrFail($id);
            $response['status'] = 'success';
            $response['message'] = 'Deleted successfully!';
            $response['comment'] = $comment;
            $statusCode = 200;
            $comment->delete();
        } catch (ModelNotFoundException $e) {
            $response['comment'] = [];
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
            $response['file'] = $e->getFile();
            $response['line'] = $e->getLine();
            $statusCode = 400;
        }
        return Response()->json($response, $statusCode);
    }

}
