<?php namespace Search\Http\Controllers;

use Search\Configuration;
use Search\Http\Requests;
use Illuminate\Database\QueryException;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Swagger\Annotations as SWG;


/**
 * @SWG\Resource(
 *    apiVersion="1.0",
 *    basePath="http://search.local/api/v1",
 *    resourcePath="/configuration",
 *    description="Post operations",
 *    produces="['application/json']"
 * )
 */
class ConfigurationController extends Controller
{

    /**
     * @SWG\Api(
     *    path="/configuration",
     *      @SWG\Operation(
     *        method="GET",
     *        summary="Display a listing of the resource.",
     *		@SWG\ResponseMessage(code=404, message="Invalid request"),
     *      @SWG\ResponseMessage(code=422, message="Basic validation failed"),
     *    )
     * )
     */
    public function index()
    {
        try {

            $response['status'] = 'success';
            $response['configuration'] = [];

            $statusCode = 200;
            $suggestions = Configuration::all();

            foreach ($suggestions as $suggestion) {
                $response['configuration'][] = $suggestion;
            }

        } catch (\Exception $e) {
            $response['status'] = 'error';
            $response['configuration'] = [];

            $statusCode = 404;
        } finally {
            return Response()->json($response, $statusCode);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, Requests\ConfigurationRequest $request)
    {
        try {
            $configuration = Configuration::findOrfail($id);
            $statusCode = 200;

            $configuration->logo = $request['logo'];
            $configuration->copyright = $request['copyright'];
            $configuration->slogan = $request['slogan'];
            $configuration->social_fb = empty($request['social_fb']) ? $configuration->social_fb : $request['social_fb'];
            $configuration->social_tw = empty($request['social_tw']) ? $configuration->social_tw : $request['social_tw'];
            $configuration->push();

            $response = [
                'status' => 'success',
                'message' => 'Record updated successfully'
            ];
        } catch (ModelNotFoundException $e) {
            $response['configuration'] = [];
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
            $response['file'] = $e->getFile();
            $response['line'] = $e->getLine();
            $statusCode = 400;
        } catch (QueryException $e) {
            $response['configuration'] = [];
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
            $response['file'] = $e->getFile();
            $response['line'] = $e->getLine();
            $statusCode = 400;
        }


        return Response()->json($response, $statusCode);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $configuration = Configuration::findOrFail($id);
            $response['status'] = 'success';
            $response['message'] = 'Deleted successfully!';
            $response['configuration'] = $configuration;
            $statusCode = 200;
            $configuration->delete();
        } catch (ModelNotFoundException $e) {
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
            $response['file'] = $e->getFile();
            $response['line'] = $e->getLine();
            $statusCode = 400;
        }
        return Response()->json($response, $statusCode);
    }

}
