<?php namespace Search\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
use Search\Http\Requests;
use Search\Posts;
use Illuminate\Http\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Swagger\Annotations as SWG;


/**
 * @SWG\Resource(
 *    apiVersion="1.0",
 *    basePath="http://search.local/api/v1",
 *    resourcePath="/posts",
 *    description="Post operations",
 *    produces="['application/json']"
 * )
 */
class PostController extends Controller
{

    /**
     * @SWG\Api(
     *    path="/posts",
     *      @SWG\Operation(
     *        method="GET",
     *        summary="Display a listing of the resource.",
     *		@SWG\ResponseMessage(code=404, message="Invalid request"),
     *      @SWG\ResponseMessage(code=422, message="Basic validation failed"),
     *    )
     * )
     */
    public function index()
    {
        try {

            $response['status'] = 'success';
            $response['posts'] = [];

            $statusCode = 200;
            $suggestions = Posts::all();

            foreach ($suggestions as $suggestion) {
                $response['posts'][] = $suggestion;
            }

        } catch (\Exception $e) {
            $response['status'] = 'error';
            $response['posts'] = [];

            $statusCode = 404;
        } finally {
            return Response()->json($response, $statusCode);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * @SWG\Api(
     *    path="/posts",
     *      @SWG\Operation(
     *        method="POST",
     *        summary="Store a newly created resource in storage.",
     *     @SWG\Parameter(
     *            name="body",
     *            description="Post information",
     *            paramType="body",
     *            required=true,
     *            allowMultiple=false,
     *            type="string",
     *            defaultValue="{""name"":""Post Name Here"",""attachments"":""dummy.png"",""content"":""Your Post content goes her""}"
     *     ),
     *      @SWG\ResponseMessage(code=422, message="Basic validation failed"),
     *     @SWG\ResponseMessage(code=200, message="Your suggestion have been saved")
     *    )
     * )
     */
    public function store(Requests\PostRequest $request)
    {
        try {
            $input = $request->all();
            $slug = strtolower(str_slug($request->input('name')) . "-" . Str::random(4));
            $url = url('/') . "posts/" . $slug;
            $additonal = [
                'slug' => $slug,
                'status' => 1,
                'url' => $url,
                'attachments' => empty($input['attachments']) ? null : $input['attachments']
            ];

            $input = array_merge($input, $additonal);
            Posts::create($input);
            $statusCode = 200;
            $response = [
                'status' => 'success',
                'message' => 'Your post have been saved'
            ];
        } catch (ModelNotFoundException $e) {
            $response['posts'] = [];
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
            $response['file'] = $e->getFile();
            $response['line'] = $e->getLine();
            $statusCode = 400;
        } catch (QueryException $e) {
            $response['posts'] = [];
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
            $response['file'] = $e->getFile();
            $response['line'] = $e->getLine();
            $statusCode = 400;
        }
        return Response()->json($response, $statusCode);
    }

    /**
     * @SWG\Api(
     *    path="/posts/{id}",
     *      @SWG\Operation(
     *        method="GET",
     *        summary="Display the specified resource.",
     *		@SWG\Parameter(
     *            name="id",
     *            description="id of suggestion to fetch",
     *            paramType="path",
     *            required=true,
     *            allowMultiple=false,
     *            type="integer"
     *        ),
     *		@SWG\ResponseMessage(code=400, message="Suggestion not found"),
     *      @SWG\ResponseMessage(code=422, message="Suggestion not found"),
     *      @SWG\ResponseMessage(code=200, message="Suggestion Found"),
     *    )
     * )
     */
    public function show($id)
    {
        try {
            $posts = Posts::findOrFail($id);
            $response['posts'] = $posts;
            $response['status'] = 'success';
            $response['message'] = '';
            $statusCode = 200;
        } catch (ModelNotFoundException $e) {
            $response['posts'] = [];
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
            $response['file'] = $e->getFile();
            $response['line'] = $e->getLine();
            $statusCode = 400;
        }
        return Response()->json($response, $statusCode);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * @SWG\Api(
     *    path="/posts/{id}",
     *      @SWG\Operation(
     *        method="PUT",
     *        summary="Update the specified resource in storage",
     *		@SWG\Parameter(
     *            name="id",
     *            description="id of author to fetch and update",
     *            paramType="path",
     *            required=true,
     *            allowMultiple=false,
     *            type="integer"
     *        ),
     *     @SWG\Parameter(
     *            name="body",
     *            description="Authors information",
     *            paramType="body",
     *            required=true,
     *            allowMultiple=false,
     *            type="string",
     *            defaultValue="{""name"":""Post Name Here"",""attachments"":""dummy.png"",""content"":""Your Post content goes her""}"
     *        ),
     *		@SWG\ResponseMessage(code=400, message="author not found"),
     *      @SWG\ResponseMessage(code=422, message="Basic validation failed"),
     *    )
     * )
     */
    public function update($id,Requests\PostRequest $request)
    {
        try {
            $post = Posts::findOrfail($id);
            $statusCode = 200;

            $post->content = $request['content'];
            $post->name = $request['name'];
            $post->attachments = empty($request['attachments']) ? $post->attachments : $request['attachments'];
            $post->push();

            $response = [
                'status' => 'success',
                'message' => 'Record updated successfully'
            ];
        } catch (ModelNotFoundException $e) {
            $response['posts'] = [];
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
            $response['file'] = $e->getFile();
            $response['line'] = $e->getLine();
            $statusCode = 400;
        } catch (QueryException $e) {
            $response['posts'] = [];
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
            $response['file'] = $e->getFile();
            $response['line'] = $e->getLine();
            $statusCode = 400;
        }


        return Response()->json($response, $statusCode);

    }

    /**
     * @SWG\Api(
     *    path="/posts/{id}",
     *      @SWG\Operation(
     *        method="DELETE",
     *        summary=" Remove the specified resource from storage.",
     *		@SWG\Parameter(
     *            name="id",
     *            description="id of post to fetch and update",
     *            paramType="path",
     *            required=true,
     *            allowMultiple=false,
     *            type="integer"
     *        ),
     *		@SWG\ResponseMessage(code=400, message="Post not found"),
     *      @SWG\ResponseMessage(code=422, message="Post not found"),
     *     @SWG\ResponseMessage(code=200, message="Post Deleted"),
     *    )
     * )
     */
    public function destroy($id)
    {
        try {
            $post = Posts::findOrFail($id);
            $response['status'] = 'success';
            $response['message'] = 'Deleted successfully!';
            $response['posts'] = $post;
            $statusCode = 200;
            $post->delete();
        } catch (ModelNotFoundException $e) {
            $response['posts'] = [];
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
            $response['file'] = $e->getFile();
            $response['line'] = $e->getLine();
            $statusCode = 400;
        }
        return Response()->json($response, $statusCode);
    }


}
