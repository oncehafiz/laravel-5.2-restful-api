<?php namespace Search\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
use Illuminate\Http\Response;
use Search\Authors;
use Search\Http\Requests\AuthorRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Swagger\Annotations as SWG;


/**
 * @SWG\Resource(
 *    apiVersion="1.0",
 *    basePath="http://search.local/api/v1",
 *    resourcePath="/authors",
 *    description="Author operations",
 *    produces="['application/json']"
 * )
 */


class AuthorController extends Controller {

    /**
     * @SWG\Api(
     *    path="/authors",
     *      @SWG\Operation(
     *        method="GET",
     *        summary="Display a listing of the resource.",
     *		@SWG\ResponseMessage(code=404, message="Invalid request"),
     *      @SWG\ResponseMessage(code=422, message="Basic validation failed"),
     *    )
     * )
     */
	public function index()
	{
        try{

            $response['status']      = 'success';
            $response['authors']     = [];

            $statusCode  = 200;
            $authors = Authors::all();

            foreach($authors as $author){
                $response['authors'][] = $author;
            }

        }catch (\Exception $e){
            $response['status']  = 'error';
            $response['authors'] = [];

            $statusCode = 404;
        }finally{
            return Response()->json($response, $statusCode);
        }
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

    /**
     * @SWG\Api(
     *    path="/authors",
     *      @SWG\Operation(
     *        method="POST",
     *        summary="Store a newly created resource in storage.",
     *     @SWG\Parameter(
     *            name="body",
     *            description="Authors information",
     *            paramType="body",
     *            required=true,
     *            allowMultiple=false,
     *            type="string",
     *            defaultValue="{""name"":""Author Name Here"",""email"":""admin@info.com""}"
     *     ),
     *
     *		@SWG\ResponseMessage(code=422, message="Basic validation failed"),
     *     @SWG\ResponseMessage(code=200, message="Your author have been saved")
     *    )
     * )
     */
	public function store(AuthorRequest $request)
	{
        $input     = $request->all();
        $slug      = strtolower(str_slug($request->input('name'))."-".Str::random(4));
        $additonal = [
            'slug' => $slug,
            'attachments' => empty($input['attachments']) ? null : $input['attachments']
        ];

        $input = array_merge($input,$additonal);
        Authors::create($input);

        $response = [
            'status'    => 'success',
            'message'   => 'Author have been saved'
        ];

        return Response()->json($response, 200);
	}

    /**
     * @SWG\Api(
     *    path="/authors/{id}",
     *      @SWG\Operation(
     *        method="GET",
     *        summary="Display the specified resource.",
     *		@SWG\Parameter(
     *            name="id",
     *            description="id of author to fetch",
     *            paramType="path",
     *            required=true,
     *            allowMultiple=false,
     *            type="integer"
     *        ),
     *		@SWG\ResponseMessage(code=400, message="author not found"),
     *      @SWG\ResponseMessage(code=422, message="author not found"),
     *      @SWG\ResponseMessage(code=200, message="author Found"),
     *    )
     * )
     */
	public function show($id)
	{
        try
        {
            $authors = Authors::findOrFail($id);
            $response['authors']     = $authors;
            $response['status']      = 'success';
            $response['message']     = '';
            $statusCode              = 200;
        }
        catch(ModelNotFoundException $e)
        {
            $response['authors'] = [];
            $response['status']      = 'error';
            $response['message']     = $e->getMessage();
            $response['file']        = $e->getFile();
            $response['line']        = $e->getLine();
            $statusCode              = 400;
        }
        return Response()->json($response, $statusCode);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

    /**
     * @SWG\Api(
     *    path="/authors/{id}",
     *      @SWG\Operation(
     *        method="PUT",
     *        summary="Update the specified resource in storage",
     *		@SWG\Parameter(
     *            name="id",
     *            description="id of author to fetch and update",
     *            paramType="path",
     *            required=true,
     *            allowMultiple=false,
     *            type="integer"
     *        ),
     *     @SWG\Parameter(
     *            name="body",
     *            description="Authors information",
     *            paramType="body",
     *            required=true,
     *            allowMultiple=false,
     *            type="string",
     *            defaultValue="{""name"":""Author Name Here"",""email"":""admin@info.com""}"
     *        ),
     *		@SWG\ResponseMessage(code=400, message="author not found"),
     *      @SWG\ResponseMessage(code=422, message="Basic validation failed"),
     *    )
     * )
     */
	public function update($id,AuthorRequest $request)
	{
        try
        {
            $authors = Authors::findOrfail($id);
            $statusCode  = 200;

            $authors->email       = $request['email'];
            $authors->name        = $request['name'];
            $authors->attachments = empty($request['attachments']) ? $authors->attachments : $request['attachments'];
            $authors->push();

            $response = [
                'status'    => 'success',
                'message'   => 'Record updated successfully'
            ];
        }
        catch(ModelNotFoundException $e)
        {
            $response['authors']     = [];
            $response['status']      = 'error';
            $response['message']     = $e->getMessage();
            $response['file']        = $e->getFile();
            $response['line']        = $e->getLine();
            $statusCode              = 400;
        }
        catch(QueryException $e)
        {
            $response['authors']     = [];
            $response['status']      = 'error';
            $response['message']     = $e->getMessage();
            $response['file']        = $e->getFile();
            $response['line']        = $e->getLine();
            $statusCode              = 400;
        }


        return Response()->json($response, $statusCode);
	}

    /**
     * @SWG\Api(
     *    path="/authors/{id}",
     *      @SWG\Operation(
     *        method="DELETE",
     *        summary=" Remove the specified resource from storage.",
     *		@SWG\Parameter(
     *            name="id",
     *            description="id of author to fetch and delete",
     *            paramType="path",
     *            required=true,
     *            allowMultiple=false,
     *            type="integer"
     *        ),
     *		@SWG\ResponseMessage(code=400, message="author not found"),
     *      @SWG\ResponseMessage(code=422, message="author not found"),
     *     @SWG\ResponseMessage(code=200, message="author Deleted"),
     *    )
     * )
     */
	public function destroy($id)
	{
        try
        {
            $authors = Authors::findOrFail($id);
            $response['status']      = 'success';
            $response['message']     = 'Deleted successfully!';
            $response['authors']     = $authors;
            $statusCode              = 200;
            $authors->delete();
        }
        catch(ModelNotFoundException $e)
        {
            $response['authors']     = [];
            $response['status']      = 'error';
            $response['message']     = $e->getMessage();
            $response['file']        = $e->getFile();
            $response['line']        = $e->getLine();
            $statusCode              = 400;
        }
        return Response()->json($response, $statusCode);
	}

}
