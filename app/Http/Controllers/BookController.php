<?php namespace Search\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
use Search\Books;
use Search\Http\Requests;
use Search\Http\Requests\BookRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Swagger\Annotations as SWG;

/**
 * @SWG\Resource(
 *    apiVersion="1.0",
 *    basePath="http://search.local/api/v1",
 *    resourcePath="/books",
 *    description="Book operations",
 *    produces="['application/json']"
 * )
 */
class BookController extends Controller
{

    /**
     * @SWG\Api(
     *    path="/books",
     *      @SWG\Operation(
     *        method="GET",
     *        summary="Display a listing of the resource.",
     *		@SWG\ResponseMessage(code=404, message="Invalid request"),
     *      @SWG\ResponseMessage(code=422, message="Basic validation failed"),
     *    )
     * )
     */
    public function index()
    {
        try {

            $response['status'] = 'success';
            $response['books'] = [];

            $statusCode = 200;
            $suggestions = Books::all();

            foreach ($suggestions as $suggestion) {
                $response['books'][] = $suggestion;
            }

        } catch (\Exception $e) {
            $response['status'] = 'error';
            $response['books'] = [];

            $statusCode = 404;
        } finally {
            return Response()->json($response, $statusCode);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * @SWG\Api(
     *    path="/books",
     *      @SWG\Operation(
     *        method="POST",
     *        summary="Store a newly created resource in storage.",
     *     @SWG\Parameter(
     *            name="body",
     *            description="Name of book",
     *            paramType="body",
     *            required=true,
     *            allowMultiple=false,
     *            type="string",
     *            defaultValue="{""name"":""Book Name Here"",""author_id"":""2""}"
     *     ),
     *		@SWG\ResponseMessage(code=422, message="Basic validation failed"),
     *     @SWG\ResponseMessage(code=200, message="Your book have been saved")
     *    )
     * )
     */
    public function store(BookRequest $request)
    {
        try {

            $input = $request->all();
            $slug = strtolower(str_slug($request->input('name')) . "-" . Str::random(4));
            $additonal = [
                'slug' => $slug,
                'attachments' => empty($input['attachments']) ? null : $input['attachments']
            ];

            $input = array_merge($input, $additonal);

            Books::create($input);
            $statusCode = 200;
            $response = [
                'status' => 'success',
                'message' => 'Book have been saved'
            ];

        } catch (ModelNotFoundException $e) {
            $response['books'] = [];
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
            $response['file'] = $e->getFile();
            $response['line'] = $e->getLine();
            $statusCode = 400;
        }
        return Response()->json($response, $statusCode);
    }

    /**
     * @SWG\Api(
     *    path="/books/{id}",
     *      @SWG\Operation(
     *        method="GET",
     *        summary="Display the specified resource.",
     *		@SWG\Parameter(
     *            name="id",
     *            description="id of book to fetch",
     *            paramType="path",
     *            required=true,
     *            allowMultiple=false,
     *            type="integer"
     *        ),
     *		@SWG\ResponseMessage(code=400, message="book not found"),
     *      @SWG\ResponseMessage(code=422, message="book not found"),
     *      @SWG\ResponseMessage(code=200, message="book Found"),
     *    )
     * )
     */
    public function show($id)
    {
        try {
            $books = Books::findOrFail($id);
            $response['status'] = 'success';
            $response['message'] = '';
            $response['books'] = $books;
            $statusCode = 200;
        } catch (ModelNotFoundException $e) {
            $response['books'] = [];
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
            $response['file'] = $e->getFile();
            $response['line'] = $e->getLine();
            $statusCode = 400;
        }
        return Response()->json($response, $statusCode);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * @SWG\Api(
     *    path="/books/{id}",
     *      @SWG\Operation(
     *        method="PUT",
     *        summary="Update the specific record from storage.",
     *     @SWG\Parameter(
     *            name="id",
     *            description="id of book to fetch and update",
     *            paramType="path",
     *            required=true,
     *            allowMultiple=false,
     *            type="integer"
     *        ),
     *     @SWG\Parameter(
     *            name="body",
     *            description="Name of book",
     *            paramType="body",
     *            required=true,
     *            allowMultiple=false,
     *            type="string",
     *            defaultValue="{""name"":""Book Name Here"",""author_id"":""2""}"
     *     ),
     *		@SWG\ResponseMessage(code=422, message="Basic validation failed"),
     *     @SWG\ResponseMessage(code=200, message="Your book have been saved")
     *    )
     * )
     */
    public function update($id, Requests\BookRequest $request)
    {
        try {
            $books = Books::findOrfail($id);
            $statusCode = 200;

            $books->name = $request['name'];
            $books->attachments = empty($request['attachments']) ? null : $request['attachments'];
            $books->push();

            $response = [
                'status' => 'success',
                'message' => 'Record updated successfully'
            ];
        } catch (ModelNotFoundException $e) {
            $response['books'] = [];
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
            $response['file'] = $e->getFile();
            $response['line'] = $e->getLine();
            $statusCode = 400;
        } catch (QueryException $e) {
            $response['books'] = [];
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
            $response['file'] = $e->getFile();
            $response['line'] = $e->getLine();
            $statusCode = 400;
        }


        return Response()->json($response, $statusCode);
    }

    /**
     * @SWG\Api(
     *    path="/books/{id}",
     *      @SWG\Operation(
     *        method="DELETE",
     *        summary=" Remove the specified resource from storage.",
     *		@SWG\Parameter(
     *            name="id",
     *            description="id of book to fetch and delete",
     *            paramType="path",
     *            required=true,
     *            allowMultiple=false,
     *            type="integer"
     *        ),
     *		@SWG\ResponseMessage(code=400, message="book not found"),
     *      @SWG\ResponseMessage(code=422, message="book not found"),
     *     @SWG\ResponseMessage(code=200, message="book Deleted"),
     *    )
     * )
     */
    public function destroy($id)
    {
        try {
            $books = Books::findOrFail($id);
            $response['status'] = 'success';
            $response['message'] = 'Deleted successfully!';
            $response['books'] = $books;
            $statusCode = 200;
            $books->delete();
        } catch (ModelNotFoundException $e) {
            $response['books'] = [];
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
            $response['file'] = $e->getFile();
            $response['line'] = $e->getLine();
            $statusCode = 400;
        }
        return Response()->json($response, $statusCode);
    }

}
