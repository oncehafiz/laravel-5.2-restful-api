<?php namespace Search\Http\Controllers;


use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
use Search\EmailQueue;
use Search\Http\Requests;
use Search\Suggestions;
use Illuminate\Http\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Swagger\Annotations as SWG;


/**
 * @SWG\Resource(
 *    apiVersion="1.0",
 *    basePath="http://search.local/api/v1",
 *    resourcePath="/suggestions",
 *    description="Suggestion operations",
 *    produces="['application/json']"
 * )
 */
class SuggestionController extends Controller
{

    /**
     * @SWG\Api(
     *    path="/suggestions",
     *      @SWG\Operation(
     *        method="GET",
     *        summary="Display a listing of the resource.",
     *		@SWG\ResponseMessage(code=404, message="Invalid request"),
     *      @SWG\ResponseMessage(code=422, message="Basic validation failed"),
     *    )
     * )
     */
    public function index()
    {
        try {

            $response['status'] = 'success';
            $response['suggestions'] = [];

            $statusCode = 200;
            $suggestions = Suggestions::all();

            foreach ($suggestions as $suggestion) {
                $response['suggestions'][] = $suggestion;
            }

        } catch (\Exception $e) {
            $response['status'] = 'error';
            $response['suggestions'] = [];

            $statusCode = 404;
        } finally {
            return Response()->json($response, $statusCode);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }


    /**
     * @SWG\Api(
     *    path="/suggestions",
     *      @SWG\Operation(
     *        method="POST",
     *        summary="Store a newly created resource in storage.",
     *     @SWG\Parameter(
     *            name="body",
     *            description="Suggestion information",
     *            paramType="body",
     *            required=true,
     *            allowMultiple=false,
     *            type="string",
     *            defaultValue="{""name"":""Suggestion Name Here"",""email"":""admin@info.com"",""content"":""Your feedback""}"
     *     ),
     *      @SWG\ResponseMessage(code=422, message="Basic validation failed"),
     *     @SWG\ResponseMessage(code=200, message="Your suggestion have been saved")
     *    )
     * )
     */
    public function store(Requests\SuggestionRequest $request)
    {
        try {
            $input = $request->all();
            $slug = strtolower(str_slug($request->input('name')) . "-" . Str::random(4));
            $ticket_id = strtoupper(Str::random(8));
            $additonal = [
                'slug' => $slug,
                'ticket_id' => $ticket_id,
                'attachments' => empty($input['attachments']) ? null : $input['attachments']
            ];

            $input = array_merge($input, $additonal);
            Suggestions::create($input);


            // Now saving email data to database
            $data['ticket_id'] = $ticket_id;
            $data['content'] = $input['content'];
            $data['name'] = $input['name'];
            $emailView = view('emails.suggestion')->with('data', $data);
            $email_queue = ['sender_name' => Config('constants.SENDER_NAME'),
                'sender_email' => Config('constants.SENDER_EMAIL'),
                'reciver_name' => $input['name'],
                'reciver_email' => $input['email'],
                'subject' => 'Feedback',
                'body' => $emailView];

            EmailQueue::create($email_queue);

            $response = [
                'status' => 'success',
                'message' => 'Your suggestion have been saved'
            ];
        } catch (ModelNotFoundException $e) {
            $response['suggestions'] = [];
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
            $response['file'] = $e->getFile();
            $response['line'] = $e->getLine();
            $statusCode = 400;
        }
        return Response()->json($response, $statusCode);


    }

    /**
     * @SWG\Api(
     *    path="/suggestions/{id}",
     *      @SWG\Operation(
     *        method="GET",
     *        summary="Display the specified resource.",
     *		@SWG\Parameter(
     *            name="id",
     *            description="id of suggestion to fetch",
     *            paramType="path",
     *            required=true,
     *            allowMultiple=false,
     *            type="integer"
     *        ),
     *		@SWG\ResponseMessage(code=400, message="Suggestion not found"),
     *      @SWG\ResponseMessage(code=422, message="Suggestion not found"),
     *      @SWG\ResponseMessage(code=200, message="Suggestion Found"),
     *    )
     * )
     */
    public function show($id)
    {
        try {
            $suggestion = Suggestions::findOrFail($id);
            $response['suggestions'] = $suggestion;
            $response['status'] = 'success';
            $response['message'] = '';
            $statusCode = 200;
        } catch (ModelNotFoundException $e) {
            $response['suggestions'] = [];
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
            $response['file'] = $e->getFile();
            $response['line'] = $e->getLine();
            $statusCode = 400;
        }
        return Response()->json($response, $statusCode);
    }


    /**
     * @SWG\Api(
     *    path="/suggestions/{id}/edit",
     *      @SWG\Operation(
     *        method="GET",
     *        summary="Show the form for editing the specified resource.",
     *		@SWG\Parameter(
     *            name="id",
     *            description="id of suggestion to fetch",
     *            paramType="path",
     *            required=true,
     *            allowMultiple=false,
     *            type="integer"
     *        ),
     *		@SWG\ResponseMessage(code=400, message="Suggestion not found"),
     *      @SWG\ResponseMessage(code=422, message="Basic validation failed"),
     *    )
     * )
     */
    public function edit($id)
    {
        try {
            $suggestion = Suggestions::findOrFail($id);
            $response['suggestions'] = $suggestion;
            $response['status'] = 'success';
            $response['message'] = '';
            $statusCode = 200;
        } catch (ModelNotFoundException $e) {
            $response['suggestions'] = [];
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
            $response['file'] = $e->getFile();
            $response['line'] = $e->getLine();
            $statusCode = 400;
        }
        return Response()->json($response, $statusCode);
    }


    /**
     * @SWG\Api(
     *    path="/suggestions/{id}",
     *      @SWG\Operation(
     *        method="PUT",
     *        summary="Update the specified resource in storage",
     *		@SWG\Parameter(
     *            name="id",
     *            description="id of suggestion to fetch and update",
     *            paramType="path",
     *            required=true,
     *            allowMultiple=false,
     *            type="integer"
     *        ),
     *     @SWG\Parameter(
     *            name="body",
     *            description="Authors information",
     *            paramType="body",
     *            required=true,
     *            allowMultiple=false,
     *            type="string",
     *            defaultValue="{""name"":""Suggestion Name Here"",""email"":""admin@info.com"",""content"":""Your feedback""}"
     *        ),
     *		@SWG\ResponseMessage(code=400, message="Suggestion not found"),
     *      @SWG\ResponseMessage(code=422, message="Basic validation failed"),
     *    )
     * )
     */
    public function update($id, Requests\SuggestionRequest $request)
    {
        try {
            $suggestions = Suggestions::findOrfail($id);
            $statusCode = 200;

            $suggestions->email = $request['email'];
            $suggestions->name = $request['name'];
            $suggestions->content = $request['content'];
            $suggestions->attachments = empty($request['attachments']) ? $suggestions->attachments : $request['attachments'];
            $suggestions->push();

            $response = [
                'status' => 'success',
                'message' => 'Record updated successfully'
            ];
        } catch (ModelNotFoundException $e) {
            $response['suggestions'] = [];
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
            $response['file'] = $e->getFile();
            $response['line'] = $e->getLine();
            $statusCode = 400;
        } catch (QueryException $e) {
            $response['authors'] = [];
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
            $response['file'] = $e->getFile();
            $response['line'] = $e->getLine();
            $statusCode = 400;
        }


        return Response()->json($response, $statusCode);
    }

    /**
     * @SWG\Api(
     *    path="/suggestions/{id}",
     *      @SWG\Operation(
     *        method="DELETE",
     *        summary=" Remove the specified resource from storage.",
     *		@SWG\Parameter(
     *            name="id",
     *            description="id of suggestion to fetch and update",
     *            paramType="path",
     *            required=true,
     *            allowMultiple=false,
     *            type="integer"
     *        ),
     *		@SWG\ResponseMessage(code=400, message="Suggestion not found"),
     *      @SWG\ResponseMessage(code=422, message="Suggestion not found"),
     *     @SWG\ResponseMessage(code=200, message="Suggestion Deleted"),
     *    )
     * )
     */
    public function destroy($id)
    {
        try {
            $suggestion = Suggestions::findOrFail($id);
            $response['status'] = 'success';
            $response['message'] = 'Deleted successfully!';
            $response['suggestions'] = $suggestion;
            $statusCode = 200;
            $suggestion->delete();
        } catch (ModelNotFoundException $e) {
            $response['suggestions'] = [];
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
            $response['file'] = $e->getFile();
            $response['line'] = $e->getLine();
            $statusCode = 400;
        }
        return Response()->json($response, $statusCode);
    }

}
