<?php

namespace Search\Http\Requests;

use Search\Http\Requests\Request;

class ConfigurationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        switch($this->method())
        {
            case 'GET':
            {
                $rules = [
                    'id'         => 'integer'
                ];
            }
                break;
            case 'DELETE':
            {
                $rules = [];
            }
                break;
            case 'POST':
            {
                $rules = [
                    'logo'            => 'required',
                    'copyright'       => 'required',
                    'slogan'          => 'required',
                    'social_fb'       => 'url',
                    'social_tw'       => 'url'
                ];
            }
                break;
            case 'PUT':
                $rules = [
                    'logo'            => 'required',
                    'copyright'       => 'required',
                    'slogan'          => 'required',
                    'social_fb'       => 'url',
                    'social_tw'       => 'url'
                ];
                break;
            case 'PATCH':
                $rules = [
                    'logo'            => 'required',
                    'copyright'       => 'required',
                    'slogan'          => 'required',
                    'social_fb'       => 'url',
                    'social_tw'       => 'url'
                ];
                break;

            default:break;
        }

        return $rules;
    }
}
