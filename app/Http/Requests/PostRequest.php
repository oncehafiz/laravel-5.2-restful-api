<?php

namespace Search\Http\Requests;

use Search\Http\Requests\Request;

class PostRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        switch($this->method())
        {
            case 'GET':
            {
                $rules = [
                    'id'         => 'integer'
                ];
            }
                break;
            case 'DELETE':
            {
                $rules = [
                    'id'         => 'required|integer'
                ];
            }
                break;
            case 'POST':
            {
                $rules = [
                    'name'       => 'required|max:25|unique:posts'
                ];
            }
                break;
            case 'PUT':
                $rules = [
                    'name'       => 'required'
                ];
                break;
            case 'PATCH':
                break;

            default:break;
        }

        return $rules;
    }
}
