<?php

namespace Search\Http\Requests;

use Search\Http\Requests\Request;
use Illuminate\Validation\Validator;

class SuggestionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        switch($this->method())
        {
            case 'GET':
            {
                $rules = [
                    'id'         => 'integer'
                ];
            }
                break;
            case 'DELETE':
            {
                $rules = [];
            }
                break;
            case 'POST':
            {
                $rules = [
                    'email'      => 'required|email',
                    'name'       => 'required'
                ];
            }
                break;
            case 'PUT':
                $rules = [
                    'email'      => 'required|email',
                    'name'       => 'required'
                ];
                break;
            case 'PATCH':
                break;

            default:break;
        }

        return $rules;
    }



}
