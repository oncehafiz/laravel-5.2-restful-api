<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);


Route::group(array('prefix' => 'api/v1'), function()
{

    Route::resource('authors', 'AuthorController');
    Route::resource('suggestions', 'SuggestionController');
    Route::resource('books', 'BookController');
    Route::resource('comments', 'CommentController');
    Route::resource('configuration', 'ConfigurationController');
    Route::resource('posts', 'PostController');
    Route::resource('emailqueue', 'EmailqueueController');

});